use escuela;

/* Mostrar tabla beca*/
describe beca;

/*Mostrar los datos de la tabla becas.*/
select * from beca;

/*¿Cuántas becas se han otorgado? */
SELECT COUNT(*) "Becas otorgadas: "
FROM beca;

/*¿Cuántas becas se han otorgado antes del año 2000?*/
SELECT COUNT(*) "Becas otorgadas antes del año 2000: "
FROM beca
WHERE YEAR(fecha) < 2000;

/*Media cuantia becas.*/
SELECT ROUND(AVG(cuantia),2) "Media cuantía becas: "
FROM beca;

/*Media cuantia becas (sólo contabilizar aquellas con un valor mayor a 1000).*/
SELECT AVG(cuantia) "Media cuantía becas mayores a 1000€: "
FROM beca
WHERE cuantia > 1000;

/*Obtener cuantas becas se han otorgado para cada cuantía*/
SELECT cuantia,
	   COUNT(*) "Numero de becas asignadas: "
FROM beca 
GROUP BY cuantia;

/*Obtener cuantas becas se han otorgado para cada cuantía siempre que se 
 haya entregado más que 1.*/
 
SELECT cuantia,
		COUNT(*) "Becas asignadas:"
FROM beca 
GROUP BY cuantia
HAVING COUNT(*) > 1;

/*Obtener cuantas becas (mayores de 1000 euros) se han otorgado para cada cuantía
 siempre que se haya entregado más que 1.*/
SELECT cuantia,
	   COUNT(*) "Becas asignadas:"
FROM beca 
WHERE cuantia > 1000
GROUP BY cuantia
HAVING COUNT(*) > 1;
 
/*¿Cuántas becas han sido concedidas por fecha?*/
SELECT fecha, 
		COUNT(*) "Becas asignadas: "
FROM beca
GROUP BY 1;

/*¿Cuál es la cuantía economica de becas por fecha?*/
SELECT fecha, 
		SUM(cuantia)"Cuantia asignadas: "
FROM beca
GROUP BY 1;

/*¿Cuál es la cuantía economica de becas y su incidendicia por fecha?*/
SELECT fecha, 
		COUNT(id) "Becas asignadas:",
		SUM(cuantia)"Cuantia asignadas: "
FROM beca
GROUP BY fecha;

/*Mostrar todos los módulos alumno.*/
SELECT * FROM modulo_alumno;

/*Mostrar las notas de los módulos alumno.*/
SELECT nota FROM modulo_alumno;

/*Media de notas.*/
SELECT AVG(nota) "Nota media:" FROM modulo_alumno;

/*Media de notas por módulo*/
SELECT cod_modulo "Código de módulo: ", 
		ROUND(AVG(nota),2) "Nota media: "
FROM modulo_alumno
GROUP BY cod_modulo;

/*Nota máxima*/
SELECT MAX(nota) "Nota más alta: "
FROM modulo_alumno;

/*¿Cuál es la nota mínima y de qué expediente alumno?*/
SELECT num_expediente "Expediente:",
		nota
FROM modulo_alumno
ORDER BY nota
LIMIT 1;

/* Información de todos los profesores. */
SELECT * FROM profesor;

/*¿Cuántos módulos da cada profesor?*/
SELECT dni,
	   p.nombre "Profesor",
       COUNT(cod_modulo) "Módulos impartidos: "
FROM profesor p JOIN modulo m ON p.dni = m.dni_profesor
GROUP BY dni;

/*El nombre de los profesores que dan matemáticas.*/
select * from modulo;
SELECT p.nombre "Profesores de matemáticas: "
FROM profesor p JOIN modulo m ON p.dni = m.dni_profesor
WHERE m.nombre = "Matematicas";

/*¿Cuántos grupos hay?*/
SELECT COUNT(*) "Numero de grupos: " FROM grupo;

/*¿Cuántas aulas hay?*/
SELECT COUNT(*) "Numero de aulas: " FROM aula;

/*¿Qué grupo no tiene aula?*/
select id,
	   cod_curso "curso",
       letra
from grupo
WHERE id NOT IN (SELECT id_grupo FROM aula);

/*Muestra el listado de todos los grupos y el número de plazas.*/
SELECT g.id "ID Grupo",
	   IFNULL(a.num_plazas, 0) "Plazas"
FROM grupo g  LEFT JOIN aula a ON g.id = a.id_grupo;

/*Mostrar las notas, y el nombre del modulo de los módulos alumno.*/
SELECT ma.num_expediente "Expediente",
	   ma.nota "Nota",
	   m.nombre "Módulo"
FROM modulo_alumno ma JOIN modulo m ON ma.cod_modulo = m.id;

/*Mostrar los nombres completos de los alumnos, sus notas y los nombres del modulo 
de los módulos alumno.*/
SELECT CONCAT(a.nombre, " ", a.apellido_1, " ", a.apellido_2) "Nombre:",
        m.nombre "Módulo",
		ma.nota "nota"
FROM modulo_alumno ma JOIN modulo m ON ma.cod_modulo = m.id
					  JOIN alumno a ON a.num_expediente = ma.num_expediente;

/*Media de las notas en matemáticas*/
SELECT  m.nombre "Módulo",
		ROUND(AVG(ma.nota),2) "Media:"
FROM modulo_alumno ma JOIN modulo m ON ma.cod_modulo = m.id
WHERE m.nombre = "Matematicas";

/*Alumnos sin nota en matemáticas*/
SELECT CONCAT(nombre, " ", apellido_1, " ", apellido_2) "Alumnos sin nota en matemáticas: "
FROM alumno
WHERE num_expediente NOT IN (SELECT num_expediente
							FROM modulo_alumno ma JOIN modulo m ON ma.cod_modulo = m.id
							WHERE m.nombre = "Matematicas");

/*Alumnos con beca y aprobados*/
SELECT CONCAT(nombre, " ", apellido_1, " ", apellido_2) "Alumnos con beca y aprobados: "
FROM alumno a JOIN modulo_alumno ma ON a.num_expediente = ma.num_expediente
WHERE ma.nota >= 5 AND a.num_expediente IN (SELECT num_exp_alumno
											FROM beca);
                                            
/*¿Cuántos alumnos aprobaron por materia?*/
SELECT m.nombre "Módulo",
	 COUNT(ma.nota) "Aprobados"
FROM modulo_alumno ma JOIN modulo m ON ma.cod_modulo = m.id
WHERE nota >= 5
GROUP BY 1;
 
/*¿En qué aulas no hay suficientes ordenadores?*/
SELECT a.cod_aula "Codigo de aula",
		a.num_ordenadores "Ordenadores",
        count(*) "Alumnos"
FROM aula a JOIN grupo g ON a.id_grupo = g.id
			JOIN alumno al ON g.id = al.id_grupo
GROUP BY 1
HAVING Ordenadores < Alumnos;

-- Mostrar alumnos del curso 1
SELECT a.num_expediente, a.nombre, a.apellido_1, a.apellido_2, g.cod_curso "curso", g.letra "grupo"
FROM alumno a JOIN grupo g on a.id_grupo = g.id
             JOIN curso c on g.cod_curso = c.cod_curso
WHERE g.cod_curso = 1;


-- Numero de modulos que imparten los profesores de las empresas

select p.cod_empresa, e.nombre, sum(x.count) as modulos from empresa e
    join profesor p on e.cod_empresa=p.cod_empresa
    join (select count(*) as count, dni_profesor from modulo group by dni_profesor) x on p.dni=x.dni_profesor group by cod_empresa;

-- Alumnos y el numero de temas que cursan

select am.num_expediente, a.nombre, count(cod_tema) as temas from alumno a
    join modulo_alumno am on a.num_expediente=am.num_expediente
    join tema t on t.cod_modulo=am.cod_modulo group by num_expediente;






