
-- beca
select * from beca;
select count(*) from beca;
select count(*) from beca where YEAR(fecha) < '2000';
select avg(cuantia) from beca;
select avg(cuantia) from beca where cuantia > 1000;
select cuantia, count(*) from beca GROUP BY cuantia;
select cuantia, count(*) as otorgadas from beca GROUP BY cuantia having otorgadas > 1;
select cuantia, count(*) as otorgadas from beca where cuantia > 1000 GROUP BY cuantia having otorgadas > 1;

select count(cuantia), fecha from beca GROUP BY fecha;
select sum(cuantia), fecha from beca GROUP BY fecha;

select  sum(cuantia), count(cuantia), fecha from beca GROUP BY fecha;

-- notas: modulo alumno
select * from modulo_alumno;
select ma.nota from modulo_alumno ma;

select min(nota), num_expediente from modulo_alumno;
select max(nota) from modulo_alumno;


select * from grupo;

select  cod_curso, letra, (SELECT count(*) from alumno a WHERE a.id_grupo=g.id) as alumnos from grupo g;

select * from 
(select g.*, a.num_plazas, a.num_ordenadores,
	(SELECT count(*) from alumno a WHERE a.id_grupo=g.id) as alumnos
	from grupo g LEFT JOIN aula a ON g.id=a.id_grupo) s where alumnos>num_ordenadores; 


SELECT  cod_curso, letra, num_plazas 
	from grupo g LEFT JOIN aula a ON g.id=a.id_grupo;
    
    
select * from aula;

-- profesores
select * from profesor p join modulo m on p.dni=m.dni_profesor ;
select count(*), p.nombre from profesor p join modulo m on p.dni=m.dni_profesor GROUP BY p.nombre;


-- complejas

select m.nombre, ma.nota from modulo_alumno ma JOIN modulo m on m.cod_modulo=ma.cod_modulo;
select CONCAT(a.nombre, " ", a.apellido_1, " ", a.apellido_2) as nombre_completo , m.nombre, ma.nota 
	from modulo_alumno ma JOIN modulo m on m.cod_modulo=ma.cod_modulo JOIN alumno a ON a.num_expediente=ma.num_expediente;
