alter table pelicula add column duracion SMALLINT unsigned default 0;

create table productora(
	id int unsigned auto_increment primary key,
    nombre varchar(50) not null
);

alter table pelicula_datos_produccion
	drop constraint pelicula_datos_produccion_id_datos_produccion_fk2;

alter table datos_produccion
	drop primary key,
    modify id int unsigned,
	add column id_pelicula int unsigned after id,
	add column fecha_inicio_produccion datetime after costes,
    add constraint datos_produccion_fecha_inicio_produccion_ck check(fecha_fin_produccion is null or (fecha_inicio_produccion<fecha_fin_produccion)),
    add column id_productora int unsigned after id,
    add constraint datos_produccion_id_productora_fk foreign key (id_productora) references productora(id);

update datos_produccion x set id_productora=(select id from productora y where x.productora=y.nombre);
update datos_produccion x set id_pelicula=(select id_pelicula from pelicula_datos_produccion y where x.id=y.id_datos_produccion);

alter table datos_produccion
	drop column id,
	drop column productora,
    modify column id_pelicula int unsigned primary key,
    add constraint datos_produccion_id_pelicula_fk foreign key (id_pelicula) references pelicula(id);

alter table pelicula_genero
	drop primary key,
    add primary key(id_pelicula, id_genero);




-- helpers
describe datos_produccion;
show create table datos_produccion;

INSERT into productora VALUes(null, 'aaa');
INSERT into productora VALUes(null, 'bbb');

INSERT into datos_produccion values(null,'aaa', DEFAULT, null);
INSERT into datos_produccion values(null,'aaa', DEFAULT, null);
INSERT into datos_produccion values(null,'bbb', DEFAULT, null);

select * from datos_produccion;

select * from pelicula;
insert into pelicula values(
	null, 'Apolo 11', 'M-13', 50, 90
);

insert into pelicula values(
	null, 'Apolo 12', 'M-13', 50, 90
);

insert into pelicula_datos_produccion values(1,1),(2,2),(3,3);
